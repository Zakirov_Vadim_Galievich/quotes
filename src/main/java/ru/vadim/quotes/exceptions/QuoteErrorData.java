package ru.vadim.quotes.exceptions;

public class QuoteErrorData {
    private String info;

    public QuoteErrorData() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
